package home.work;

public class CustomEntity {
  private long id;
  private String name;
  private String email;

  public CustomEntity(long id, String name, String email) {
    this.id = id;
    this.name = name;
    this.email = email;
  }

  public String getEmail() {
    return email;
  }
  public String getName() {
    return name;
  }

  @Override
  public String toString() {
    return "CustomEntity{" +
        "id=" + id +
        ", name='" + name + '\'' +
        ", email='" + email + '\'' +
        '}';
  }

  // TODO implement equality by email only
  @Override
  public boolean equals(Object o) {
    return true;
  }

  // TODO
  @Override
  public int hashCode() {
    return 0;
  }
}
