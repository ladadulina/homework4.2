package home.work;

import org.junit.Test;

import static org.junit.Assert.*;

public class CustomQueueTest {

  @Test
  public void testCustomEqualsMethod() {
    CustomEntity bombur1 = new CustomEntity(1, "Bombur", "bombur@gmail.com");
    CustomEntity bombur2 = new CustomEntity(2, "Bombur", "bombur2@gmail.com");
    CustomEntity bombur3 = new CustomEntity(1, "Bombur", "bombur3@gmail.com");
    CustomEntity bombur4 = new CustomEntity(3, "Bombur", "bombur@gmail.com");

    assertEquals(bombur1, bombur4);
    assertNotEquals(bombur1, bombur2);
    assertNotEquals(bombur1, bombur3);
    assertNotEquals(bombur1, null);
    assertNotEquals(bombur1, "bombur@gmail.com");
  }

  @Test
  public void checkForNullTest() {
    CustomEntity test1 = new CustomEntity(1, "Corn", "corn@hub.com");
    CustomEntity nullTest = new CustomEntity(1, "Corn", null);

    assertNotEquals(test1, nullTest);
    assertNotEquals(test1, null);
    assertNotEquals(nullTest, null);
  }

  @Test
  public void checkForCasesAndSpacesTest() {
    CustomEntity test1 = new CustomEntity(1, "Corn", "corn@hub.com");
    CustomEntity test2 = new CustomEntity(1, "Corn", "corN@hub.com");
    CustomEntity testSpaces = new CustomEntity(3, "space", "    ");
    CustomEntity testTabs = new CustomEntity(4, "tab", "\t@test.com");
    CustomEntity testSingleSpace = new CustomEntity(4, "tab", " @test.com");
    CustomEntity testMultiSpace = new CustomEntity(4, "tab", "  @test.com");

    assertNotEquals(test1, test2);
    assertNotEquals(testSpaces, testTabs);
    assertNotEquals(testSingleSpace, testMultiSpace);
  }

  @Test
  public void checkForEmailEqualsTest() {
    CustomEntity test1 = new CustomEntity(1, "Corn", "corn@hub.com");
    CustomEntity test2 = new CustomEntity(1, "Corn", "master.corn@hub.commmand.edu".substring(7,19));

    assertEquals(test1, test2);
  }

  @Test
  public void testCustomHashCodeMethod() {
    CustomEntity bombur1 = new CustomEntity(1, "Bombur", "bombur@gmail.com");
    CustomEntity bombur2 = new CustomEntity(2, "Bombur", "bombur2@gmail.com");
    CustomEntity bombur3 = new CustomEntity(1, "Bombur", "bombur3@gmail.com");
    CustomEntity bombur4 = new CustomEntity(3, "Bombur", "bombur@gmail.com");

    assertEquals(bombur1.hashCode(), bombur4.hashCode());
    assertNotEquals(bombur1.hashCode(), bombur2.hashCode());
    assertNotEquals(bombur1.hashCode(), bombur3.hashCode());
    assertNotEquals(bombur2.hashCode(), bombur3.hashCode());
  }

  @Test
  public void testCustomQueue() {
    CustomPriorityQueue<CustomEntity> customQueue = new CustomPriorityQueue<>();
    CustomEntity bombur1 = new CustomEntity(1, "Bombur", "bombur@gmail.com");
    CustomEntity bifur = new CustomEntity(2, "Bifur", "bifur@gmail.com");
    CustomEntity bimbur = new CustomEntity(3, "Bimbur", "bimbur@gmail.com");
    CustomEntity bombur2 = new CustomEntity(4, "Bombur", "bombur2@gmail.com");
    CustomEntity bibur = new CustomEntity(5, "Bibur", "bibur@gmail.com");

    customQueue.add(bombur1);
    customQueue.add(bifur);
    customQueue.add(bimbur);
    customQueue.add(bombur2);
    customQueue.add(bombur1);

    assertEquals(bifur, customQueue.poll());
    assertEquals(bimbur, customQueue.poll());
    assertEquals(bombur2, customQueue.poll());
    assertEquals(bombur1, customQueue.poll());
    assertNull(customQueue.poll());

    assertEquals(1, customQueue.getEnterCount(bombur2));
    assertEquals(2, customQueue.getEnterCount(bombur1));
    assertEquals(0, customQueue.getEnterCount(bibur));

    customQueue.add(bombur1);
    assertEquals(3, customQueue.getEnterCount(bombur1));
    assertEquals(bombur1, customQueue.poll());

    assertEquals(2, customQueue.getPollCount(bombur1));
    assertEquals(1, customQueue.getPollCount(bombur2));
    assertEquals(0, customQueue.getPollCount(bibur));
  }

  @Test
  public void testCustomQueueAddAndOffer() {
    //persons preparing
    CustomPriorityQueue<CustomEntity> customQueue = new CustomPriorityQueue<>();
    CustomEntity xiaoWu = new CustomEntity(2, "Xiao Wu Student", "XiaoWu@shrek.academy.edu");
    CustomEntity oscar = new CustomEntity(4, "Oscar Student", "Oscar@shrek.academy.edu");
    CustomEntity bibiDong = new CustomEntity(5, "Bibi Dong", "BibiDong@spirit.hall.org");
    CustomEntity daiMubai = new CustomEntity(3, "Dai Mubai Student", "DaiMubai@Star.Luo.Empire.gov");
    CustomEntity starLuoEmpirePrince = new CustomEntity(6, "Dai Mubai", "DaiMubai@Star.Luo.Empire.gov");
    CustomEntity zhuZhuqing = new CustomEntity(5, "Zhu Zhuqing Student", "ZhuZhuqing@Star.Luo.Empire.gov");
    CustomEntity starLuoEmpirePrincess = new CustomEntity(6, "Zhu Zhuqing", "ZhuZhuqing@Star.Luo.Empire.gov");
    CustomEntity ningRongrong = new CustomEntity(6, "Ning Rongrong", "NingRongrong@Nine-Colors.org");
    CustomEntity tangSan = new CustomEntity(1, "Tang San Student", "TangSan@Tan.clan.com");
    CustomEntity tangSanStudent = new CustomEntity(6, "Tang San", "TangSan@shrek.academy.edu");
    CustomEntity tanClanMaster = new CustomEntity(6, "Tang Yin", "TangSan@Tan.clan.com");

    //add and offer persons to customQueue
    assertTrue(customQueue.add(tangSan));
    assertTrue(customQueue.add(xiaoWu));
    assertTrue(customQueue.add(daiMubai));
    assertTrue(customQueue.add(oscar));
    assertTrue(customQueue.add(bibiDong));
    assertTrue(customQueue.offer(tangSanStudent));
    assertFalse(customQueue.offer(tanClanMaster));

    //check queue size
    assertEquals(6, customQueue.size());

    //check poll of persons
    assertEquals(bibiDong, customQueue.poll());
    assertEquals(daiMubai, customQueue.poll());
    assertEquals(oscar, customQueue.poll());
    assertEquals(tangSan, customQueue.poll());
    assertEquals(tangSanStudent, customQueue.poll());
    assertEquals(xiaoWu, customQueue.poll());
    assertNull(customQueue.poll());

    //check enters and pools
    assertEquals(0, customQueue.getEnterCount(ningRongrong));
    assertEquals(0, customQueue.getPollCount(ningRongrong));
    assertEquals(1, customQueue.getEnterCount(oscar));
    assertEquals(1, customQueue.getPollCount(oscar));
    assertEquals(1, customQueue.getEnterCount(daiMubai));
    assertEquals(1, customQueue.getPollCount(daiMubai));
    assertEquals(2, customQueue.getEnterCount(tangSan));
    assertEquals(1, customQueue.getPollCount(tangSan));
    assertEquals(1, customQueue.getEnterCount(tangSanStudent));
    assertEquals(1, customQueue.getPollCount(tangSanStudent));
    assertEquals(1, customQueue.getEnterCount(xiaoWu));
    assertEquals(1, customQueue.getPollCount(xiaoWu));
    assertEquals(0, customQueue.getEnterCount(zhuZhuqing));
    assertEquals(0, customQueue.getPollCount(zhuZhuqing));

    //second wave
    assertTrue(customQueue.offer(zhuZhuqing));
    assertTrue(customQueue.add(daiMubai));
    assertFalse(customQueue.add(starLuoEmpirePrince));
    assertFalse(customQueue.offer(starLuoEmpirePrincess));

    //check that duplicates not added
    CustomEntity soulLandPerson = customQueue.poll();
    assertEquals(soulLandPerson, daiMubai);
    assertEquals(soulLandPerson.getName(), daiMubai.getName());
    assertNotEquals(soulLandPerson.getName(), "DaiMubai@Star.Luo.Empire.gov");
    assertNotEquals(soulLandPerson.getName(), starLuoEmpirePrince.getName());

    soulLandPerson = customQueue.poll();
    assertEquals(soulLandPerson, starLuoEmpirePrincess);
    assertEquals(soulLandPerson.getEmail(), zhuZhuqing.getEmail());
    assertEquals(soulLandPerson.getName(), zhuZhuqing.getName());
    assertNotEquals(soulLandPerson.getName(), starLuoEmpirePrincess.getName());
    assertNull(customQueue.poll());

    //check enter counts
    assertEquals(1, customQueue.getEnterCount(bibiDong));
    assertEquals(3, customQueue.getEnterCount(starLuoEmpirePrince));
    assertEquals(2, customQueue.getEnterCount(starLuoEmpirePrincess));
    assertEquals(2, customQueue.getEnterCount(zhuZhuqing));

    //check poll counts
    assertEquals(2, customQueue.getPollCount(daiMubai));
    assertEquals(1, customQueue.getPollCount(zhuZhuqing));
  }

  @Test
  public void testConstructorWithCapacity() {
    CustomPriorityQueue<CustomEntity> customQueue = new CustomPriorityQueue<>(4);
    CustomEntity bombur1 = new CustomEntity(1, "Bombur", "bombur@gmail.com");
    CustomEntity bifur = new CustomEntity(2, "Bifur", "bifur@gmail.com");
    CustomEntity bimbur = new CustomEntity(3, "Bimbur", "bimbur@gmail.com");
    CustomEntity bombur2 = new CustomEntity(4, "Bombur", "bombur2@gmail.com");

    customQueue.add(bombur1);
    customQueue.add(bifur);
    customQueue.add(bimbur);
    customQueue.add(bombur2);
    customQueue.add(bombur1);

    assertEquals(bifur, customQueue.poll());
    assertEquals(bimbur, customQueue.poll());
    assertEquals(bombur2, customQueue.poll());
    assertEquals(bombur1, customQueue.poll());
    assertNull(customQueue.poll());
  }
}